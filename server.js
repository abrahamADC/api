//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movientosJSON = require('./movimientosv2.json');

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname,'index.html'));
  //res.send('Hola Mundo Node JS API');
})

app.get('/Clientes', function(req,res) {
  res.send('Aqui estan los clientes devueltos nuevos');
})

app.get('/Clientes:idCliente', function(req,res) {
  res.send('Aqui tiene al cliente: '+req.params.idCliente);
})

app.get('/Movimientos/v1', function(req,res) {
  res.sendFile(path.join(__dirname,'movimientosv1.json'));
})

app.get('/Movimientos/v2/:index', function(req,res) {
  console.log(req.params.index);
  res.send(movientosJSON[req.params.index-1]);

//  res.sendfile('movimientosv2.json');
})

//app.get('/Movimientos/v2', function(req,res) {
//  res.sendFile(path.join(__dirname,'movimientosv2.json'));
//})

app.get('/Movimientos/v2', function(req,res) {
//  res.sendFile(path.join(__dirname,'movimientosv2.json'));
  res.send(movientosJSON[req.params.index-1]);
})



app.get('/Movimientosq/v2', function(req,res) {
  console.log('Movimientosq');
  console.log(req.query);
  res.send('recibido');

//  res.sendfile('movimientosv2.json');
})


app.post('/', function(req,res) {
  res.send('Hemos recibido su peticion POST');
})

app.post('/movimientosv2', function(req,res) {
var nuevo = req.body;
nuevo.id  = movientosJSON.length+1;
movientosJSON.push(nuevo);
  res.send('Movimiento dado de Alta');
})
